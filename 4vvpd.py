def random_array(length, m):
    import random

    return [[random.randint(0, 255) for _ in range(length[1])] for _ in range(length[0])]


def input_array(length, m):
    if m:
        if len(m) == length[0] * length[1]:
            return [m[(i * length[1]):((i + 1) * length[1])] for i in range(length[0])] 
        else:
            print('(m) Ошибка ввода: длина не соответствует заданной')
    else: 
        print('(m) Ошибка ввода: не введен массив')
    return False
    

def intcount(matrix, end):
    x, y = 0, 0
    x2, y2 = end
    if x2 < x or y2 < y:
        return 0
    return matrix[y2][x2] - intcount(matrix, (x2 - 1, y2 - 1)) + intcount(matrix, (x2 - 1, y2)) + intcount(matrix, (x2, y2 - 1))


def count_all_array(matrix, strat, end):
    print('Результат выполнения выбранной функции:', intcount(matrix, (len(matrix[0]) - 1, len(matrix) - 1)))


def count_part_array(matrix, start, end):
    if start and end:
        try:
            start = (int(start[0]), int(start[1]))
            end = (int(end[0]), int(end[1]))
        except IndexError:
            print('(start, end) Ошибка ввода: нужно ввести x и y в каждом')
        else:
            if all(el > 0 for el in start and end) and all(start[i] <= end[i] for i in range(2)):
                if start[0] <= len(matrix) and start[1] <= len(matrix[0]) and end[0] <= len(matrix) and end[1] <= len(matrix[0]):
                    matrix = [[matrix[i][j] for i in range(start[0], end[0] + 1)] for j in range(start[1], end[1] + 1)]
                    count_all_array(matrix, start, end)
                else:
                    print('(start, end) Ошибка ввода: в матрице нет таких кординат начала или конца')
            else:
                print('(start, end) Ошибка ввода: кординаты начала > координат конца или не получены натуральные числа')
    else:
        print('(start, end) Ошибка ввода: не получены обязаельные для этой функции переменные')


MENU1 = {'1': input_array, '2': random_array}
MENU2 = {'1': count_all_array, '2': count_part_array}


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-a', help='''Аргумент отвечает за тип генерации матрицы
        \'1\' - вручную, \'2\' - случайные числа''')
    parser.add_argument('-y', type=int, help='Кол-во строк в матрице')
    parser.add_argument('-x', type=int, help='Кол-во столбцов в матрице')
    parser.add_argument('--m', nargs='+', type=int, help='''Матрица с y строк и x столбцов
        При случаной генерации массива значение аргумента не важно''')
    parser.add_argument('-func', help='''Функция, которую хотите выбрать
        \'1\' - расчет матрицы интегрального представления(всей матрицы)
        \'2\' - сумма пикселей в прямоугольнике, ограниченном заданными точками''')
    parser.add_argument('--start', nargs='+', type=int, help='''координаты начала отсчета
        кол-во строк и кол-во столбцов через пробел (для функции 2). Начинать с 0''')
    parser.add_argument('--end', nargs='+', type=int, help='''координаты конца отсчета
        кол-во строк и кол-во столбцов через пробел (для функции 2). Начинать с 0''')
    args = parser.parse_args()
    if args.a in MENU1 and args.func in MENU2:
        if args.a and args.y and args.x > 0 and args.y > 0:
            matrix = MENU1[args.a]((args.y, args.x), args.m)
            if matrix:
                print('Сгенерированный массив:')
                for el in matrix:
                    print(el)
                MENU2[args.func](matrix, args.start, args.end)
                
        else:
            print('(x, y) Ошибка ввода: не получены натуральные числа')
    else:
        print('''(a, func) Ошибка ввода: не получены обязательные параметры,
        или их значения неверны''')


main()
